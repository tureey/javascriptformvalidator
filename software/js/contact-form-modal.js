$(document).ready(function(){
	/* Event after click on menu icon */
	$('#toggle-icon').click(function(){
		$('#content').fadeOut('fast');
		$('div.title-bar').fadeOut('fast');
		$('#content header').fadeOut('fast');
		$('#content >h3').fadeOut('fast');
		$('#example').fadeOut('fast');
		$('#modal-menu').fadeIn('slow');
	});
	$('#contact-modal-close').click(function(){
		$('#modal-menu').fadeOut('fast');
		$('#content').fadeIn('slow');
		$('div.title-bar').fadeIn('slow');
		$('#content header').fadeIn('slow');
		$('#content >h3').fadeIn('slow');
		$('#example').fadeIn('slow');
	});
});