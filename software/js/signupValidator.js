/*
** Aqui irán todas las funciones y eventos relacionados con la validación y detección de
** código en las cadenas de userName, summonerName, password.

			-----	TODOS LOS CAMPOS SÓN REQUIRED 	-----

** @userName->  				- No caracteres especiales
								- Alfanumérico

** @summonerName-> Segun RIOT:  - Entre 4 i 24 carácteres
								- Alfanumérico

** @password ->		            - Alfanumérico
								- Ha de coincidir con la confirmación
								- Entre 8 i x carácteres
*/


// El dolar en frente de una variable sirve para indicar que son objetos jQuery
var $userName 			= $('#userName');
var $summonerName 		= $('#summonerName');
var $password 			= $('#password')
var $passwordConfirm 	= $('#password-confirm');
var $email 				= $('#email')

$(document).ready(function(){


/*******************************************
**       Eventos de onkeyup para
**   informar al momento de los errores
*/

// userName
	// Validamos que el tenga un formato correcto, en caso negativo mostramos un mensaje de error
	$('#userName').keyup(function(){
		if(!isValidName($userName.val()))
			indicarErrorUserName();
		else
			solucionarErrorUserName();
	});

// summonerName
	// Validamos que tenga un formato correcto, en caso negativo mostramos un mensaje de error
	$('#summonerName').keyup(function(){
		if(!isValidName($summonerName.val()))
			indicarErrorSummonerName();
		else{
			solucionarErrorSummonerName();
		}
	});

// Email
	// Validamos que tenga un formato correcto, en caso negativo mostramos un mensaje de error
	$('#email').keyup(function(){
		if(!isValidEmailAddress($email.val()))
			indicarErrorEmail();
		else{
			solucionarErrorEmail();
		}
	});

// Password
	// Comprobamos que lascontraseñas tengan el mismo valor y sean alfanumericas
	$('#password').keyup(function(){
		// Si uno de los dos cudros no tiene valor
		if($password.val() === "" || $passwordConfirm.val() === ""){
			indicarErrorPasswordVacio();
		}
		
		// Si los dos cuadros tienen valor pero no sn iguales y correctos
		else if(!areValidPasswords($password.val(), $passwordConfirm.val())
			|| $password.val() === "" || $passwordConfirm.val() === ""){
			indicarErrorPasswords();
		}

		// Si todo OK
		else
			solucionarErrorPasswords();
	});

// Password Confirm
	// Comprobamos exaactamente lo mismo cuando detectamos actividad en el cuadro de confirmación de contraseña
	$('#password-confirm').keyup(function(){
		// Si uno de los dos cudros no tiene valor
		if($('#password').val() === "" || $('#password-confirm').val() === ""){
			indicarErrorPasswordVacio();
		}
		
		// Si los dos cuadros tienen valor pero no sn iguales y correctos
		else if(!areValidPasswords($('#password').val(), $('#password-confirm').val())
			||$('#password').val() === "" || $('#password-confirm').val() === ""){		
			indicarErrorPasswords();
		}

		// Si todo OK
		else
			solucionarErrorPasswords();
	});
});

/**************************************
**         Funcion que evita el
**     formulario en caso de error
*/

// Tan solo validamos el formulario en caso de completar todos los requisitos, en caso contrario mostraremos los distintos mensajes
function isValidForm(){
	if(isNotBlank($userName.val()) && isNotBlank($summonerName.val()) && isNotBlank($email.val()) && isNotBlank($password.val()) && isNotBlank($passwordConfirm.val())
		&& isAlphaNumeric($userName.val()) && isAlphaNumeric($summonerName.val()) && isAlphaNumeric($password.val()) && isAlphaNumeric($passwordConfirm.val())
		&& checkMinMaxCharacters('name', $userName.val()) && checkMinMaxCharacters('name', $summonerName.val())
		&& checkMinMaxCharacters('password', $password.val()) && checkMinMaxCharacters('password', $passwordConfirm.val())
		&& isEqualString($password.val(), $passwordConfirm.val()) && isValidEmailAddress($email.val())){
		return true;
	}
	else
	{// Miramos los valores sección a sección para mostrar los mensajes de error correspondientes
		// Check userName
		if(!isNotBlank(userNameVal) || !isAlphaNumeric(userNameVal) || !checkMinMaxCharacters('userName', userNameVal))
			indicarErrorUserName();
		else
			solucionarErrorUserName();

		// Check summonerName
		if(!isNotBlank(summonerName) || !isAlphaNumeric(summonerName) || !checkMinMaxCharacters('summnerName', summonerName))
			indicarErrorSummonerName();
		else
			solucionarErrorSummonerName();
		
		// Check email
		if(!isValidEmailAddress(emailVal))
			indicarErrorEmail();
		else
			solucionarErrorEmail();

		// Check passwords
		if(isAlphaNumeric(passwordVal) || isAlphaNumeric(passworConfirmdVal) || checkMinMaxCharacters('password', passwordVal)
			|| checkMinMaxCharacters('password', passwordConfirmVal)|| isEqualString(passwordVal, passwordConfirmVal))
			indicarErrorPasswords();
		else
			solucionarErrorPasswords();

		// Check terms of Use
		if($('#termsOfUse').prop('checked', false))
			indicarErrorTermsOfUse();
		else
			solucionarErrorTermsOfUse();

		// cancelamos el submit
		return false;
	}
}


/*
** Check the number of characters included on this word.
**
** @label  -> Indicates the form zone (userName,summonerName,password,email)
** @string -> The string to check
*/function checkMinMaxCharacters(label, cadena){

	if ((label == "name" 		&& cadena.length >= 4 &&  cadena.length <= 24) || 	// Entre 4-y caracteres
		(label == "password"	&& cadena.length >= 8 &&  cadena.length <= 20))	{ 	// Entre 8-x caracteres
		return true;
	}
	else{ // Independientemente del que sea, estará mal
		return false;
	}
}

/*
** Miramos que se cumplan todas las condicions para que una contraseña sea correcta. Alfanumerico, caracteres minimos e iguales
**
*/function areValidPasswords(password, passwordConfirm){
	if(isEqualString(password,passwordConfirm) && checkMinMaxCharacters("password", password)
		&& checkMinMaxCharacters("password", passwordConfirm) && password != "" && passwordConfirm != "")
		return true;
	else
		return false;

}

function isValidName(cadena){
	if(isNotBlank(cadena) && isAlphaNumeric(cadena) && checkMinMaxCharacters('name', cadena))
		return true;
	else
		return false;
}
// Comprobamos que las cadenas sean las mismas
function isEqualString(cadena1, cadena2){
	if(cadena1 == cadena2)
		return true;
	else
		return false;
}

// Compara si es null o no
function isNotBlank(cadena){
	if(cadena != "")
		return true;
	else
		return false;

}
/*
** Miramos que una cadena sea alphanumerica o no.
**
** @return TRUE  -> En caso que SI que lo sea
** @return FALSE -> En caso que NO que lo sea
*/function isAlphaNumeric(alphane)
{
	var numaric = alphane;
	var cont    = 0;
  
	for(var j=0; j<numaric.length; j++)
	{
	  var alphaa = numaric.charAt(j);
	  var hh = alphaa.charCodeAt(0);

	  if((hh > 47 && hh<58) || (hh > 64 && hh<91) || (hh > 96 && hh<123))
	  {
	   cont++; 
	  }
	}
	if(cont == numaric.length)
	  return true;
	else 
	  return false;
}

/*
** Miramos que el email correspondiente tenga un formato correcto
**
** @return TRUE  -> En caso que SI que lo sea
** @return FALSE -> En caso que NO que lo sea
*/function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}







/******************************************************************************
**
** Mensajes de error en el formulario Como también su función para borrarlos
**
**/
function indicarErrorUserName(){
	// Borramos por si hay mensajes anteriores
	$('.userName .formNotValid').remove();
	// Printeamos el recuadro de color rojo
	$('#signup-modal .row label input#userName').css('box-shadow','inset 0 0 10px red');
	// Escribimos un mensaje para que el usuario sepa el error
	$('.userName legend').append('<small class="formNotValid"> (Requisitos: Entre 4 y 24 carácteres alfanuméricos)</small>');
}
function solucionarErrorUserName(){
	// Borramos por si hay mensajes anteriores
	$('.userName .formNotValid').remove();
	// Printeamos el recuadro de color rojo
	$('#signup-modal .row label input#userName').css('box-shadow','none');
}


function indicarErrorSummonerName(){
	// Borramos por si hay mensajes anteriores
	$('.summonerName .formNotValid').remove();
	// Printeamos el recuadro de color rojo
	$('#summonerName').css('box-shadow','inset 0 0 10px red');
	// Escribimos un mensaje para que el usuario sepa el error
	$('.summonerName legend').append('<small class="formNotValid"> (Requisitos: Entre 4 y 24 carácteres alfanuméricos)</small>');
}
function solucionarErrorSummonerName(){
	// Borramos por si hay mensajes anteriores
	$('.summonerName .formNotValid').remove();
	// Printeamos el recuadro de color rojo
	$('#signup-modal .row label input#summonerName').css('box-shadow','none');
}


function indicarErrorEmail(){
	// Borramos por si hay mensajes anteriores
	$('.email .formNotValid').remove();
	// Printeamos el recuadro de color rojo
	$('#email').css('box-shadow','inset 0 0 10px red');
	// Escribimos un mensaje para que el usuario sepa el error
	$('.email legend').append('<small class="formNotValid"> (Requisitos: Entre 4 y 24 carácteres alfanuméricos)</small>');
}
function solucionarErrorEmail(){
	// Borramos por si hay mensajes anteriores
	$('.email .formNotValid').remove();
	// Printeamos el recuadro de color rojo
	$('#signup-modal .row label input#email').css('box-shadow','none');
}


function indicarErrorPasswords(){
	// Borramos por si hay mensajes anteriores
	$('.password > small.formNotValid').remove();
	// Printeamos los dos recuadros de color rojo
	$('#password').css('box-shadow','inset 0 0 10px red');
	$('#password-confirm').css('box-shadow','inset 0 0 10px red');
	// Escribimos un mensaje para que el usuario sepa el error
	$('.password').append('<small class="formNotValid" style="display:block;text-align:center;font-size:70%">'
								+'(Requisitos: Entre 4 y 24 carácteres alfanuméricos)'
							+'</small>');
}
function indicarErrorPasswordVacio(){
	// Borramos por si hay mensajes anteriores
	$('.password > small.formNotValid').remove();
	// Printeamos los dos recuadros de color rojo
	$('#password').css('box-shadow','inset 0 0 10px red');
	$('#password-confirm').css('box-shadow','inset 0 0 10px red');
	// Escribimos un mensaje para que el usuario sepa el error
	$('.password').append('<small class="formNotValid" style="display:block;text-align:center;font-size:70%">'
								+"(Requisitos: Te falta rellenar el otro cuadro de 'contraseña')"
							+'</small>');
}
function solucionarErrorPasswords(){
	// Borramos por si hay mensajes anteriores
	$('.password .formNotValid').remove();
	// Printeamos el recuadro de color rojo
	$('#signup-modal .row label input#password').css('box-shadow','none');
	$('#signup-modal .row label input#password-confirm').css('box-shadow','none');
}


function indicarErrorTermsOfUse(){
	// Cambiamos de negro a rojo las letras de aceptar las conciciones de uso y subrallamos los links
	$('#confirm p').css('color','red');
	$('#confirm p').css('font-style','italic');
	$('#confirm a').css('color', 'red');
	$('#confirm a').css('font-style','italic');
	$('#confirm a').css('text-decoration', 'underline');
}
function solucionarErrorTermsOfUse(){
	// Cambiamos de negro a rojo las letras de aceptar las conciciones de uso y subrallamos los links
	$('#confirm p').css('color','black');
	$('#confirm p').css('font-style','none');
	$('#confirm a').css('color', '#1585cf');
	$('#confirm a').css('font-style','none');
	$('#confirm a').css('text-decoration', 'none');
}

function checkUrlHash(){


	var url = window.location.href;
	
	var splited = url.split('/');

	var last = splited[splited.length - 2];

	if(last == 'signup')
		$('#signup-btn').click();

	if(last == 'login')
		$('#login-btn').click();
}

//Check if must open modal
$(document).on('ready', checkUrlHash);