$(document).ready(function(){
// Cambiamos el contenido de la web al clicar en los botones
$('button.btn-download').click(function() {
	$(this).removeClass('-border');
	$('button.btn-demo').removeClass('-border').addClass('-border');
	$('#example')
});

$('button.btn-demo').click(function() {
	$(this).removeClass('-border');
	$('button.btn-download').removeClass('-border').addClass('-border');
});



// Animaciones de texto/estilo cuando el input:focus
	$('#form-example input').focus(function(){
		$(this).prev().addClass('c-salmon');		// adding salmon color
		$(this).prev().css('font-weight', 'bold');	// Transforming font into bold
		$(this).next().addClass('c-salmon');		// adding salmon color
		$(this).next().css('font-weight', 'bold');	// Transforming font into bold
	});
	$('#form-example input').focusout(function(){
		$(this).prev().removeClass('c-salmon');		// Removing salmon color
		$(this).prev().css('font-weight', 'bold');	// btn-download bold style
		$(this).next().removeClass('c-salmon');		// Removing salmon color
		$(this).next().css('font-weight', 'bold');	// btn-download bold style
	});
});